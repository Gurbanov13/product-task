package az.ingress.product.repository;

import az.ingress.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ProductRepository extends JpaRepository<Product,Long> {

    Set<Product> findAllByPriceGreaterThan(Double price);
    Set<Product> findAllByCategoriesNameEquals(String category);

}
