package az.ingress.product.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ProductDto {

    Long id;
    String name;
    BigDecimal price;
    Integer count;
    Set<CategoryDto> categoryDto;
    ManufacturerDto manufacturerDto;
}
