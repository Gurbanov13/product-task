package az.ingress.product.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ManufacturerDto {

    Long id;
    String name;
    String aboutUs;
}
