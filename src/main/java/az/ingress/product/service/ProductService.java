package az.ingress.product.service;

import az.ingress.product.dto.ProductDto;

import java.util.Set;

public interface ProductService {

    public Set<ProductDto> getAllProducts();

    public ProductDto save (ProductDto dto);

    public Set<ProductDto> getAllProductsByPriceOver (Double price);

    public Set<ProductDto> getProductWithCategory(String category);
}
