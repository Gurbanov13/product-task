package az.ingress.product.service;

import az.ingress.product.dto.CategoryDto;
import az.ingress.product.dto.ManufacturerDto;
import az.ingress.product.dto.ProductDto;
import az.ingress.product.model.Category;
import az.ingress.product.model.Manufacturer;
import az.ingress.product.model.Product;
import az.ingress.product.repository.CategoryRepository;
import az.ingress.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private  final CategoryRepository categoryRepository;
    private  final ModelMapper modelMapper;
    @Override
    public Set<ProductDto> getAllProducts() {
        Set<ProductDto> collect = productRepository.findAll().stream().map(product -> {
            ProductDto productDto = modelMapper.map(product, ProductDto.class);
            ManufacturerDto manufacturerDto =modelMapper.map(product.getManufacturer(),ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto>categoryDtos=product.getCategories().stream().map(category2 -> {
                CategoryDto categoryDto=modelMapper.map(category2,CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDto(categoryDtos);
            return productDto;
        }).collect(Collectors.toSet());
        return collect;
    }

    @Override
    public ProductDto save(ProductDto dto) {
        Product product = modelMapper.map(dto, Product.class);
        Manufacturer manufacturer = modelMapper.map(dto.getManufacturerDto(), Manufacturer.class);
        product.setManufacturer(manufacturer);
        Set<Category> categories =dto.getCategoryDto().stream().map(categoryDto -> {
            Category category = modelMapper.map(categoryDto, Category.class);
            return category;
        }).collect(Collectors.toSet());
        Set<Category> newCategory=new HashSet<>();
        for (Category category:categories ){
            if (!categoryRepository.existsByName(category.getName())){
                newCategory.add(category);
            }

        }
        product.setCategories(newCategory);
        productRepository.save(product);
        return dto;
    }

    @Override
    public Set<ProductDto> getAllProductsByPriceOver(Double price) {
        Set<Product> productSet = productRepository.findAllByPriceGreaterThan(price);
        Set<ProductDto> list = productSet.stream().map(product -> {
            ProductDto productDto = modelMapper.map(product, ProductDto.class);
            ManufacturerDto manufacturerDto =modelMapper.map(product.getManufacturer(),ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto>categoryDtos=product.getCategories().stream().map(category -> {
                CategoryDto categoryDto=modelMapper.map(category,CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDto((categoryDtos));
            return productDto;
        }).collect(Collectors.toSet());
        return list;
    }


    @Override
    public Set<ProductDto> getProductWithCategory(String category) {
        Set<Product> products = productRepository.findAllByCategoriesNameEquals(category);
        Set<ProductDto> list =products.stream().map(product -> {
            ProductDto productDto = modelMapper.map(product, ProductDto.class);
            ManufacturerDto manufacturerDto = modelMapper.map(product.getManufacturer(), ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto> categoryDtos = product.getCategories().stream().map(category1 -> {
                CategoryDto categoryDto = modelMapper.map(category1, CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDto(categoryDtos);
            return productDto;
        }).collect(Collectors.toSet());
        return  list;
    }
}
