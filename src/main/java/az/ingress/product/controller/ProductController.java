package az.ingress.product.controller;


import az.ingress.product.dto.ProductDto;
import az.ingress.product.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {
    private final ProductServiceImpl productService;


    @GetMapping("/list")
    public Set<ProductDto> getAllProducts(){
        return productService.getAllProducts();
    }

    @GetMapping
    public Set<ProductDto> getAllProductsByPriceOver(@RequestParam Double price){
        return productService.getAllProductsByPriceOver(price);
    }

    @GetMapping("/category")
    public Set<ProductDto>getProductWithCategory(@RequestParam String category){
        return productService.getProductWithCategory(category);
    }
    @PostMapping
    public ProductDto save (@RequestBody ProductDto dto){
        return productService.save(dto);
    }



}

