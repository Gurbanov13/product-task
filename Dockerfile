FROM openjdk:17-alpine
RUN apk add --no-cache openjdk11
COPY build/libs/product-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-jar", "/app/product-0.0.1-SNAPSHOT.jar"]
